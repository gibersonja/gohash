package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

/*
MD5
SHA-1
RIPEMD-160
Whirlpool
SHA-2
SHA-3
BLAKE2
BLAKE3
*/

func main() {
	var data []byte

	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}
		err := scanner.Err()
		er(err)
	}

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "-h" || arg == "--help" {
			fmt.Print("-h | --help\tPrint this help message.\n")
			fmt.Print("-f | --file\tPath to file to hash.\n")
			fmt.Print("<STDIN>\tAlso accepts data to hash as STDIN pipe.\n")

			fmt.Printf("Example:\n%s...\n", os.Args[0])
			return
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
